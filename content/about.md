---
title: About
type: page
comments: true
bigimg: [{src: "/img/sphere.jpg", desc: ""}]
type: page
---

The Bionic Harp.ist is a collaborative research-creation project by harpist [Alexandra Tibbitts]({{< relref "team.md#alex" >}}) and music technologist [John Sullivan]({{< relref "team.md#johnny" >}}). The work began in 2017 with an exploratory investigation around gesture, sound and electroacoustic harp performance, and has evolved into the development of bespoke hardware to augment the concert harp and a responsive system for Tibbitts to mix acoustic harp with sampling, looping and processing of audio and visual content for immersive live solo & collaborative performance.

![Alex harp motion capture session](/img/alex-harp-mocap.webp)

- [Learn more](https://www.johnnyvenom.com/project/research/gestural-control-of-augmented-instrumental-performance/) about Alex and Johnny's original project "Gestural Control of Augmented Instrumental Performance". 

![Bionic Harp CAD design](/img/bionic_harp_cad_finished.jpg)

- [Learn more](https://www.johnnyvenom.com/project/research/bionic-harpist/) about the research behind development of the Bionic Harp.

<br><br>