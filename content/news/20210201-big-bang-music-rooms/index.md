---
title:  "The Bionic Harpist @ Big Bang Festival Ottawa"
date: 2021-02-01T10:30:52-04:00
description: 
type: post
tags: ["performance"]
bigimg: [{src: "/img/20210201-big-bang-banner.jpg"}]
thumbnail: "bigbang-thumb.jpg"
summary: “Waiting for you on the big empty stage is Alex Tibbitts. She is the bionic harpist. You’ve probably never heard a harp sounding like this. Thanks to a mysterious electronic device connected to her instrument, Alex makes her strings sound like… something else.”

---

<br><br>

> “Waiting for you on the big empty stage is Alex Tibbitts. She is the bionic harpist. You’ve probably never heard a harp sounding like this. Thanks to a mysterious electronic device connected to her instrument, Alex makes her strings sound like… something else.”

<br><br>

{{< figure src="bigbang.jpg" caption="The Bionic Harpist @ BIG BANG Festival Ottawa. 📸 : Sébastien Croteau">}}
{{< load-photoswipe >}}

<!-- ![The Bionic Harpist @ BIG BANG Festival Ottawa, photo: Sébastien Croteau](#) -->

## Video:

{{< youtube VDHlymgApTc>}}

<br><br>

### Link: 

- [BIG BANG! Music Rooms - National Arts Centre of Canada](https://nac-cna.ca/en/video/alex-tibbitts-bb2021)

<br><br>