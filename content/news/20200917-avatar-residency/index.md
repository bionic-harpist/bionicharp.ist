---
title: "AVATAR residency"
date: 2020-09-17T11:47:49-04:00
type: post
tags: ["mutek", "avatar", "residency"]
bigimg: [{src: "/img/MUTEK_performance_header.png"}]
thumbnail: "avatar-thumb.jpg"

---

To prepare for MUTEK, Alex spent the month of August in residence at [Avatar](https://www.avatarquebec.org/en/) in Québec City, working to develop her unique solo audiovisual performance. 

{{< figure src="avatar-performance.jpg" caption="Alex performing at Avatar. 📸 : Charles Emile-Beullac | Avatar" >}}
{{< load-photoswipe >}}

The month concluded with a 5à7 performance and talk. Avatar has released a short video about the project:

{{< vimeo 456190261 >}}

Read more about Alex's residency and the Avatar centre [here](https://www.avatarquebec.org/en/#!/en/projects/bionic-harp-alex-tibbitts/?t=residency). 

The Bionic Harpist would like say a huge thank you to the entire Avatar team for making this residency possible! 