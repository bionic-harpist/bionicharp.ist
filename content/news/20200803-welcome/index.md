---
title:  "Welcome"
date: 2020-08-03T23:06:15-04:00
tags: []
bigimg: [{ src: "/img/hexagon.jpg" }]
thumbnail: "hex-thumb.jpg"
summary: "Hello and welcome! The Bionic Harpist is a collaborative project between harpist and composer Alexandra Tibbitts, and John Sullivan, music technology researcher and digital luthier."
type: post

---

Hello and welcome to the [bionicharp.ist](http://bionicharp.ist) website. We're just getting going, so please check back soon for loads of new content. 

This project is a collaborative effort between Alexandra Tibbitts, *the Bionic Harpist*, and John Sullivan, music technology researcher and digital luthier. 