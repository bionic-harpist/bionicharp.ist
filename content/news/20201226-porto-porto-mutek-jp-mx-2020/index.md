---
title:  "Porto Porto debut at MUTEK.JP + MX"
date: 2020-12-26T10:51:48-04:00
description: 
type: post
tags: ["mutek", "performance", "portoporto"]
bigimg: [{src: "/img/porto-mxjp-banner.jpg"}]
thumbnail: "porto-mxjp-thumb.jpg"
summary: "Alex's new group Porto Porto performed their inaugural show for MUTEK.JP+MX in December 2020."

---

{{< figure src="porto-mxjp-full.jpg" caption="Porto Porto debut performance for MUTEK.JP+MX. 📸 : Bruno Destombes | MUTEK Montreal" >}}
{{< load-photoswipe >}}

The group presented their first performance as Porto Porto during the 2020 hybrid edition of MUTEK.JP+MX as part of the MUTEK Connect initiative.

## Video:

{{< youtube lKoLsHZus-c >}}

<!-- Image CR: Bruno Destombes | MUTEK Montreal -->

