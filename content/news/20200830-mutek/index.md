---
title:  "Bionic Harpist @ MUTEK"
date: 2020-08-30T12:46:09-04:00
tags: ["mutek", "performance", "SAT"]
bigimg: [{src: "/img/MUTEK_spectrum_2.jpg", desc: ""}]
summary: "The Bionic Harpist will be performing at MUTEK 2020 folloing a monthlong residency at AVATAR, a Québec City centre for research and development of audiovisual artwork."
type: post
thumbnail: "mutek2020-thumb.jpg"
---

![mutek banner](mutek_banner1.jpg)

The Bionic Harpist will be performing at [MUTEK 2020](https://mutek.org) in Montreal on Wednesday, September 9th. The performance represents the culmination of a monthlong residency at AVATAR, a Québec City centre for research and development of audiovisual artwork. 

See Alex's MUTEK bio page [here](https://montreal.mutek.org/en/artists/the-bionic-harpist). 

The performance is part of the **Experience 1** program in the [Satosphere](https://sat.qc.ca/en/satosphere) at the [Société des artes technologiques](https://sat.qc.ca). Oddly, at the moment the show is not listed on the MUTEK calendar, though that may have something to do with the other performer on the bill who has yet to be announced and is _a very big deal..._ 😜 

Regardless, due to COVID-19 restrictions there are no tickets available for the public to see the show in person, **_HOWEVER_**, MUTEK is set to stream all performances online so everyone should be able to experience this very special audiovisual performance. 

Check the [MUTEK website](https://mutek.org) for more up to date show and streaming information. 