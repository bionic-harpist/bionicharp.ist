---
title:  "Mutek Experience 1 announced"
date: 2020-09-03T10:30:09-04:00
type: post
tags: ["mutek", "performance", "SAT"]
bigimg: [{src: "/img/mutek-experience-1.jpg"}]
thumbnail: "mutek2020-thumb.jpg"
summary: "MUTEK Montreal has announced Experience 1, featuring the Bionic Harpist and Bleue (aka Montréal/indie legend Patrick Watson and Mishka Stein), Wed. Sept 9th at SAT."

---

MUTEK has now announced [Experience 1](https://montreal.mutek.org/en/shows/2020/experience-1/), featuring the Bionic Harpist and Bleue (aka Montréal/indie legend [Patrick Watson](http://patrickwatson.net/) and Mishka Stein). 

- **Date:** Wednesday, September 9, 2020
- **Time:** 21h00 EDT
- **Location:** Société des arts technologiques (SAT)
- **The Bionic Harpist:** [MUTEK bio](https://montreal.mutek.org/en/artists/the-bionic-harpist/)
- **Bleue:** [MUTEK bio](https://montreal.mutek.org/en/artists/patrick-watson/) 

Because of occupancy restrictions due to social distancing protocols around COVID-19, only a handful of tickets were released for this show and it is officially **SOLD OUT**. 

However, according to MUTEK's [website](https://montreal.mutek.org): 

> All local and international audiovisual performances will be presented free of charge from Tuesday, September 8 at 1 pm EST until Sunday, September 13 on a virtual platform tailor-made by MUTEK in collaboration with agency Folklore: [virtual.mutek.org](https://virtual.mutek.org).

Read all about the platform and programming for the Hybrid 21st Edition of the MUTEK Festival [here](https://mutek.org/en/news/edition-21).