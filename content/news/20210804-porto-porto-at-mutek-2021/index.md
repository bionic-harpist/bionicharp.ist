---
title:  "Porto Porto at MUTEK Montréal 2021"
date: 2021-08-04T16:34:22-04:00
type: post
tags: ["portoporto", "mutek", "mtelus"]
bigimg: [{src: "/img/mutek_2021_gradient.jpg"}]
thumbnail: "porto-porto.jpg"
summary: "Porto Porto will be a part of the MUTEK Montréal 2021 lineup as part of the Nocturne 1 concert at MTELUS on August 27th. They will be joined by bassist and composer Milo Johnson (Busty and the Bass) and several other guests."


---

![Porto Porto MUTEK banner](porto-banner.jpg)

## UPCOMING SHOW: 

- Porto Porto
- MUTEK Montréal 2021
- 22:00 August 27th, 2021 @ MTelus
- [info](https://montreal.mutek.org/en/schedule/program?date=27.08) | [tickets](https://mtelus.com/fr/spectacles/mutek-nocturne-1)

<br><br>

{{< rawhtml >}}
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/track=505238665/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="https://portoporto.bandcamp.com/track/tonico">tonico by Porto Porto!</a></iframe>
{{< /rawhtml >}}

## About Porto Porto

**Porto Porto** is making their own brand of experimental avant-house techno. Montréal based, the group is a melting pot of musical styles and cultural backgrounds, featuring an eclectic group of electronic musicians, sound artists and music producers. Together, they weave bass-heavy beats and lush melodies in an unconventional ensemble form. The group features **Alex Tibbitts** from California on the Bionic Harp, **Quan** from Vietnam on his custom-designed modular synths, **Samito** from Mozambique on the keys and vocals, and James, a.k.a. **Boogieman** & Montréal-local, on modular beats, bass lines and sound design. James and Quan, the group’s sound designers and beat makers, introduce elements from their own field recordings.

For MUTEK Montréal’s 22nd edition, they invite bassist and composer Milo Johnson (Busty and the Bass). He will be grounding the ever-soaring and experimentally derived Porto Porto vibes with bass lines, as well as composing horn and string parts for the various movements of the show which will be played live on stage by a who's who of surprise musical guests.
