---
title: Team
type: page
comments: true
bigimg: [{src: "/img/hexagon_inv.jpg"}]
type: page
---

# Alex

**Performer, composer, creative director**

{{< figure src="/img/team-alex.jpg">}}
{{< load-photoswipe >}}

California-born, Montréal-based Alex Tibbitts is The Bionic Harpist.  Classically trained, Tibbitts has since been dedicated to new forms of gestural and digital augmentation of the harp. Through real-time processing, she joins her instrument in a symbiotic dance, a metaphorical image made even more relevant by the fact that half of her body contains metal.  

Tibbitts’ unveiled her solo project, The Bionic Harpist, at the Société des arts technologique Société in MUTEK Montreal 2020.  She was the artist-in-residence at Quebec City’s Avatar Arts Center. In 2021, Tibbitts was an invited artist at the BIG BANG Festival, presented by the National Arts Center of Canada.  Spring of 2021, she presented “Émergence” , a solo recital for harp and live electronics with a special world premiere of  “Part 1: Emergence”, a continuation of the bionic harp.ist project.

Tibbitts’ work has made her a sought-out artist & guest speaker in the field, having given presentations at notable institutions such as the Université de Montréal, Conservatoire de Musique à Montréal, University of Toronto, Centre for Research Music Media and Technology at McGill University, Conservatorio Nacional de Música & Centro de Investigación y Estudios de la Música in Mexico City. 

Tibbitts holds a Masters in Interpretation as well as a Diploma in Orchestral Repertoire from l’Université de Montréal.  She is a member of McGill University’s Centre for Interdisciplinary Research in Music Media and Technology (CIRMMT) (2016).  Her augmented concert harp, designed in collaboration with John Sullivan, a digital luthier and music technology researcher, lends itself for enriching haptic and audio/visual exploration.   Together, they have published academic articles dedicated to their shared research, such as [Gestural Control of Augmented Instrumental Performance: A Case Study of the Concert Harp](https://www.johnnyvenom.com/publication/sullivan-2018/) (2018). 

Tibbitts is an active musician and arts organizer in Montréal.  She is a founding member of Ensemble ILÉA, an electroacoustic improvisation ensemble (2016).  With her experimental techno group Porto Porto,  she performed in MUTEK Japan 2020 , MUTEK Mexico 2020, and MUTEK Montréal 2021. She is an organizing member of the Montréal Contemporary Music Lab (2017) and Artistic Operations Coordinator for Orchestre de l’Agora (2018).


# Johnny

**Hardware design and fabrication; research lead**

{{< figure src="/img/team-johnny.jpg" width="400px">}}

John Sullivan is a music technology researcher whose work focuses on user-centered design of new instruments and technologies for music and multimedia performance. With a background in music performance and human-computer interaction, his work includes user research, participatory design workshops and collaborations with musicians to better understand and support professional performance practices. Additionally he has conducted research in the areas of motion capture analysis of live performance, haptic interaction, and accessible digital musical instrument design.

Under the name Johnny Venom, he has been a part of multiple indie rock groups from the northeastern US. He released several albums with various projects and has toured extensively in the US, Canada, and Europe.

Johnny holds a Ph.D. in Music Technology from McGill University in Montreal, Canada. 

Read more about Johnny's work and research at www.johnnyvenom.com. 

---

# Archive >> AVATAR 2020

In 2020, Alex completed a [residency at Avatar]({{< ref "/news/20200917-avatar-residency" >}}), a centre in Québec City dedicated to the research, creation, circulation and dissemination of audio and electronic artworks and artists. She was joined by visual and multimedia artists Marine Chavet and Nathalie LeBlanc in development of a live audiovisual performance for the Montreal MUTEK festival in September 2020. 

## Marine

**Visual design and development**

**[Marine Chavet](http://www.marinechavet.com/)** is a multidisciplinary artist. She studied graphic design and visual art, and is a hypnosis practitioner. Her work is now shifting towards performance where she swings between visual, writing, sound and hypnosis. Through the transformative power specific to each one, she questions our limits, our relation to the world, to society and to politics. Behind an apparent poetry, she imposes pauses, slows down, opening up new perspectives that take the form of acts of resistance.

## Nathalie

**Visual and interaction design; mapping software**

**[Nathalie LeBlanc](http://www.nathalie-leblanc.com/)** is a visual and interdisciplinary artist, creating video montages and collages by modifying images, sounds and objects. She alters photographs, movie sequences, images taken from webcams, reproductions or her own recordings. Her work is motivated by a desire to investigate that of grasping, of reworking and exhausting the content or the form of a moment. By different processes of transformation and using systems based on random chance, she reworks the object of her experimentations with the expectation of diverting it.

