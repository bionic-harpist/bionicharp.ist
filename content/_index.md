The Bionic Harpist is a collaborative research-creation project by harpist [Alexandra Tibbitts]({{< relref "team.md#alex" >}}) and music technologist [John Sullivan]({{< relref "team.md#johnny" >}}).  

{{< youtube VDHlymgApTc >}}

<!-- **MUTEK 2020 teaser**

---- -->

<!-- [![24H-weekend](/img/24H-la-harpiste-bionique.jpg)](https://www.24heures.ca/2020/09/02/la-harpiste-bionique-comme-dans-un-film-de-science-fiction)

Project featured in [24h Montréal](https://www.24heures.ca/2020/09/02/la-harpiste-bionique-comme-dans-un-film-de-science-fiction) on September 3rd, 2020. -->
