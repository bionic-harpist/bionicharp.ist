---
title: "Video"
date: 2020-08-21T14:35:00-04:00
draft: false
bigimg: [{src: "/img/triangle.jpg", desc: ""}]
type: page
---

## Georgia Spiropoulos — Roll...n’Roll...n’Roll, for harp and live electronics

{{< youtube A-LJwl7OxP0 >}}

<br><br>

-----

<br>

## The Bionic Harpist - BIG BANG Festival Ottawa

{{< youtube VDHlymgApTc>}}

<br><br>

-----

<br>

## PORTO PORTO  — MUTEK.JP + MX 2020

{{< youtube lKoLsHZus-c>}}

<br><br>

-----

<br>

## The Bionic Harpist - live@CIRMMT 

{{< youtube 7ikh1ijgHuA >}}

**_live@CIRMMT_** 13-February 2020, Montréal, QC

First performance with custom harp controllers developed by John Sullivan for the Bionic Harpist project.

-----

## Code d'accèss

{{< youtube Fg3SnDaxcKY >}}

**_Sessions — Épisode 3 : Le deuil de l'empreinte_**, June 2018

Composition: Kevin Girronay 

-----

## Gestural Control of Augmented Harp Performance

{{< youtube mFhUL_LdftY >}}

Documentation of early electroacoustic harp performance research, [IDMIL](http://idmil.org)/[CIRMMT](http://www.cirmmt.org), 2016/17

-----

## ...prends moi, chaos, dans tes bras...

{{< vimeo 269375405 >}}

Debut performance with gestural controllers from the project "Gestural Control of Augmented Harp Performance". 

Composition: Brice Gatinet

-----





